﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SiMay.Core
{
    public enum OperFlag
    {
        GetStartupItems,
        AddStartupItem,
        RemoveStartupItem
    }
}
